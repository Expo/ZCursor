/*
 * LE byteArray implementation
 * by Svetlana Linuxenko <linuxenko@yahoo.com>
 * rewritten by Expo (https://codeberg.org/Expo)
 */

// why we're using this instead of just directly using uint8array i shall have no clue
export class ByteArray extends Uint8Array {
  public static ensureByteArray(
    arr: ByteArray | Uint8Array | Iterable<number>,
  ) {
    if (arr instanceof ByteArray) return arr;
    else return new ByteArray(arr);
  }
  public cmp(a: ByteArray, b?: ByteArray) {
    if (!b) {
      b = a;
      a = this;
    }
    return (
      a.filter((c, i) => {
        return c === b![i];
      }).length === a.length
    );
  }
  public toInt(a: Uint8Array = this.slice(this.off, 4)) {
    return (
      (a[0] & 0xff) |
      ((a[1] << 8) & 0xffff) |
      ((a[2] << 16) & 0xffffff) |
      ((a[3] << 24) & 0xffffffff)
    );
  }
  public toBytes(int: number) {
    return new ByteArray([
      int & 0xff,
      (int >> 8) & 0xff,
      (int >> 16) & 0xff,
      (int >> 24) & 0xff,
    ]);
  }
  public nextInt() {
    return this.toInt(this.slice(this.off, (this.off += 4)));
  }
  public nextIntBytes() {
    return ByteArray.ensureByteArray(this.slice(this.off, (this.off += 4)));
  }
  public insertInt(int: number) {
    this.insertBytes(this.toBytes(int));
  }
  public insertBytes(bytes: ByteArray, length = bytes.length ?? 4) {
    this.set(bytes, this.off);
    this.off += length;
  }
  public off = 0;
}

export default ByteArray;
