import ByteArray from './bytearray';

describe('ByteArray', () => {
  test('ensureByteArray should return a ByteArray instance', () => {
    const byteArray = new ByteArray([1, 2, 3]);
    const result = ByteArray.ensureByteArray(byteArray);

    expect(result).toBeInstanceOf(ByteArray);
    expect(result).toEqual(byteArray);
  });

  test('cmp should compare two ByteArrays', () => {
    const byteArray1 = new ByteArray([1, 2, 3]);
    const byteArray2 = new ByteArray([1, 2, 3]);
    const byteArray3 = new ByteArray([1, 2, 4]);

    expect(byteArray1.cmp(byteArray2)).toBe(true);
    expect(byteArray1.cmp(byteArray3)).toBe(false);
  });

  test('toInt should convert a Uint8Array to a number', () => {
    const byteArray = new ByteArray([1, 0, 0, 0]);
    const result = byteArray.toInt();

    expect(result).toBe(1);
  });

  test('toBytes should convert a number to a ByteArray', () => {
    const number = 256;
    const byteArray = new ByteArray([0, 1, 0, 0]);
    const result = byteArray.toBytes(number);

    expect(result).toEqual(byteArray);
  });
});
