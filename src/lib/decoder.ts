import ByteArray from './bytearray';
import { SIGNATURE, IMAGE_HEADER } from './utils';

type int = number;

export type ImageInfo = {
  type: number;
  subtype: number;
  width: number;
  height: number;
  xhot: number;
  yhot: number;
  delay: number;
  start: number;
};

/**
 * Xcur decoder
 *
 * @name Decoder
 * @class
 * @access public
 * @param {Array} data array of data bytes
 */
export class Decoder {
  public data: ByteArray;
  public constructor(data: Uint8Array | Iterable<number>) {
    this.data = ByteArray.ensureByteArray(data);
    if (!this.isCursor()) throw new Error('No Xcur header found');
  }
  public isCursor() {
    return SIGNATURE.cmp(this.data.nextIntBytes());
  }
  /**
   * Get number of images
   *
   * @name images
   * @function
   * @access public
   * @returns {int}
   */
  public images(): int {
    this.data.off = 12;
    return this.data.nextInt();
  }

  /**
   * Get entry information
   *
   * @name imageInfo
   * @function
   * @access public
   * @param {int} num number of entry
   * @returns {ImageInfo} Image information e.g dimenstions, delay etc.
   */
  public imageInfo(num: int): ImageInfo | undefined {
    this.data.off = num * 12 + 16;

    if (!IMAGE_HEADER.cmp(this.data.nextIntBytes())) return;

    this.data.nextInt();
    this.data.off = this.data.nextInt();

    if (
      this.data.nextInt() !== 36 ||
      !IMAGE_HEADER.cmp(this.data.nextIntBytes())
    )
      return;

    return {
      type: this.data.nextInt(),
      subtype: this.data.nextInt(),
      width: this.data.nextInt(),
      height: this.data.nextInt(),
      xhot: this.data.nextInt(),
      yhot: this.data.nextInt(),
      delay: this.data.nextInt(),
      start: this.data.off,
    };
  }

  /**
   * Get image pixel data by entry number
   *
   * @name getDataByNum
   * @function
   * @access public
   * @param {int} num Number of entry
   * @returns {Uint8Array} pixel data
   */
  public getDataByNum(num: int): Uint8Array {
    const data = this.imageInfo(num);
    if (!data) throw new Error('failed to get data');
    return this.getData(data);
  }

  /**
   * Get image data by imageInfo
   *
   * @name getData
   * @function
   * @access public
   * @param {ImageInfo} info info created by imageInfo()
   * @returns {Uint8Array} pixels
   */
  public getData(info: ImageInfo): Uint8Array {
    return this.data.slice(
      info.start,
      info.start + info.width * info.height * 4,
    );
  }

  /**
   * Available image types
   *
   * @name imageTypes
   * @function
   * @access public
   * @returns {number[]} array of available image types
   */
  public imageTypes(): Array<number> {
    const types: number[] = [];

    for (let i = 0; i < this.images(); i++) {
      this.data.off = i * 12 + 20;
      const type = this.data.nextInt();
      if (types.indexOf(type) === -1) types.push(type);
    }

    return types;
  }

  /**
   * Get images by type
   *
   * @name getByType
   * @function
   * @access public
   * @param {number} type type of the images
   * @returns {Array<ImageInfo | undefined>} array of all available images with specified type
   */
  public getByType(type: number): Array<ImageInfo | undefined> {
    const images: (ImageInfo | undefined)[] = [];

    for (var i = 0; i < this.images(); i++) {
      this.data.off = i * 12 + 20;
      if (this.data.nextInt() === type) images.push(this.imageInfo(i));
    }

    return images;
  }
}

export default Decoder;
