import ByteArray from './bytearray';
import { SIGNATURE, IMAGE_HEADER } from './utils';
import type { ImageInfo as DecoderImageInfo } from './decoder';

export type ImageInfo = DecoderImageInfo & {
  data: ByteArray | Uint8Array;
};

/*
 *   image : {
 *      type : 48,
 *      subtype :
 *      width: 48,
 *      height: 48,
 *      xhot : 5,
 *      yhot : 7,
 *      delay : 50,
 *      data : pixels
 *      }
 */

/**
 * Xcur Encoder
 *
 * @name Encoder
 * @class
 * @access public
 * @param {Array} images array of image objects
 */
export class Encoder {
  public images = [] as ImageInfo[];
  public constructor(
    images?: Partial<ImageInfo> &
      {
        type: ImageInfo['type'];
        data: ImageInfo['data'];
      }[],
  ) {
    images = images ?? [];
    for (let i = 0; i < images.length; i++) {
      this.addImage(images[i]);
    }
  }

  /**
   * Append a single image into images array
   *
   * @name addImage
   * @function
   * @access public
   * @param {Object} image image object
   */
  public addImage(
    image: Partial<ImageInfo> & {
      type: ImageInfo['type'];
      data: ImageInfo['data'];
    },
  ) {
    this.images.push({
      type: image.type,
      subtype: image.subtype || 1,
      width: image.width || image.type,
      height: image.height || image.type,
      xhot: image.xhot || 0,
      yhot: image.yhot || 0,
      delay: image.delay || 50,
      data: image.data,
    } as ImageInfo);
  }
  private _imagesSize(images: ImageInfo[]) {
    return images.reduce((a, b) => {
      return a + 48 + b.width * b.height * 4;
    }, 16);
  }
  private _imagePos(images: ImageInfo[], position: number) {
    return (
      12 * images.length +
      16 +
      images
        .filter((i, p) => p < position)
        .reduce((a, b) => a + 36 + b.width * b.height * 4, 0)
    );
  }

  /**
   * Pack images into Xcur stream
   *
   * @name pack
   * @function
   * @access public
   * @param {ImageInfo[] | null} images
   * @returns {Uint8Array} packed Xcur data
   */
  public pack(images?: ImageInfo[] | null): Uint8Array {
    this.images = images || this.images;
    const data = new ByteArray(this._imagesSize(this.images));

    /* Insert magic header */
    data.insertBytes(SIGNATURE);
    data.insertInt(16);
    data.insertInt(1);
    data.insertInt(this.images.length);

    /* Insert ntoc headers */
    for (let i = 0; i < this.images.length; i++) {
      data.insertBytes(IMAGE_HEADER);
      data.insertInt(this.images[i].type);
      data.insertInt(this._imagePos(this.images, i));
    }

    /* Insert images */
    for (let i = 0; i < this.images.length; i++) {
      data.insertInt(36);
      data.insertBytes(IMAGE_HEADER);
      data.insertInt(this.images[i].type);
      data.insertInt(this.images[i].subtype);
      data.insertInt(this.images[i].width);
      data.insertInt(this.images[i].height);
      data.insertInt(this.images[i].xhot);
      data.insertInt(this.images[i].yhot);
      data.insertInt(this.images[i].delay);
      data.insertBytes(ByteArray.ensureByteArray(this.images[i].data));
    }

    return new Uint8Array(data);
  }
}

export default Encoder;
